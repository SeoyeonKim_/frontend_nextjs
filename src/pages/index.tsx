import Link from 'next/link'

export default function Home() {
  return (
    <Link href="/factory">
      <h1>factory 페이지로 이동</h1>
    </Link>
  )
}
